import React, { Component } from 'react';
import Header from '../common/header/Header';
import Wiki from './Wiki';

class WikiPage extends Component {
    render() {
        return (
            <React.Fragment>
                <Header />
                <Wiki />
            </React.Fragment>
        );
    }
}

export default WikiPage;