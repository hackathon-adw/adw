import React, { Component } from 'react';
import Header from '../common/header/Header';
import DashboardDrawer from './DashboardDrawer';

class DashboardPage extends Component {
    render() {
        return (
            <React.Fragment>
                <Header />
                <DashboardDrawer />
            </React.Fragment>
        );
    }
}

export default DashboardPage;