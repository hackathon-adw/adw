import React, {Component} from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import {Divider, Box, Card, CardContent, CardActions, Button, Typography, Toolbar, Drawer, Hidden, IconButton, List, ListItem, ListItemIcon, ListItemText} from '@material-ui/core/';
import Dashboard from '@material-ui/icons/Dashboard';
import AccountCircle from '@material-ui/icons/AccountCircle';
import { makeStyles } from '@material-ui/core/styles';
import {withStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import MissionTable from '../mission/MissionTable';
import ValidateTable from '../validatePoint/ValidateTable';
import {Link} from 'react-router-dom';

const drawerWidth = 240;

const styles = theme => ({
    root: {
        display: 'flex',
    },
    drawer: {
        [theme.breakpoints.up('sm')]: {
        width: drawerWidth,
        flexShrink: 0,
        },
    },
    appBar: {
        [theme.breakpoints.up('sm')]: {
        width: `calc(100% - ${drawerWidth}px)`,
        marginLeft: drawerWidth,
        },
    },
    menuButton: {
        marginRight: theme.spacing(2),
        [theme.breakpoints.up('sm')]: {
        display: 'none',
        },
    },
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
    link: {
        textDecoration: 'none'
    }
});

class DashboardDrawer extends Component {
    drawer = () => {
        const {classes} = this.props;

        return (
            <div>
                <div className={classes.toolbar} />
                <Divider />
                <List>
                    {['Dashboard', 'Mon profil', 'Les missions'].map((text, index) => (
                        <ListItem button key={text}>
                            <ListItemIcon>{index % 2 === 0 ? <Dashboard /> : <AccountCircle />}</ListItemIcon>
                            <ListItemText primary={text} />
                        </ListItem>
                    ))}
                </List>
            </div>
        );
    }
            
    render() {
        const {classes} = this.props;
        return (
            <div className={classes.root}>
            <CssBaseline />
            <nav className={classes.drawer} aria-label="mailbox folders">
                <Hidden smUp implementation="css">
                <Drawer
                    classes={{
                    paper: classes.drawerPaper,
                    }}
                >
                    {this.drawer()}
                </Drawer>
                </Hidden>
                <Hidden xsDown implementation="css">
                <Drawer
                    classes={{
                    paper: classes.drawerPaper,
                    }}
                    variant="permanent"
                    open
                >
                    {this.drawer()}
                </Drawer>
                </Hidden>
            </nav>
            <main className={classes.content}>
                <Box mb={-7}>
                    <Typography component="h1" variant="h2" color="textPrimary">
                        Mon Dashboard
                    </Typography>
                </Box>
                <div className={classes.toolbar} />
                <Grid container spacing={3}>
                    <Grid item sm={12}>
                        <Card className={classes.root}>
                            <CardContent>
                                <Typography component="h2" variant="h3" className={classes.title} gutterBottom>
                                    Jean Dujardin
                                </Typography>
                                <Typography variant="h5" color="textSecondary" component="h4">
                                    Certifié
                                </Typography>
                                <Typography variant="h5" color="textSecondary" component="h4">
                                   SIRET : 362 521 879 00034
                                </Typography>
                            </CardContent>
                        </Card>
                    </Grid>
                    <Grid item sm={12}>
                        <Card className={classes.root}>
                            <CardContent>
                                <Typography component="h2" variant="h3" className={classes.title} gutterBottom>
                                    Mes missions
                                </Typography>
                                <MissionTable />
                            </CardContent>
                        </Card>
                    </Grid>
                    <Grid item sm={12}>
                        <Card className={classes.root}>
                            <CardContent>
                                <Typography component="h2" variant="h3" className={classes.title} gutterBottom>
                                    Mes points de validation
                                </Typography>
                                <ValidateTable />
                            </CardContent>
                        </Card>
                    </Grid>
                </Grid>
            </main>
            </div>
        );
    }
}

export default withStyles(styles)(DashboardDrawer);