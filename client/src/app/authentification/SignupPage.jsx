import React, {Component} from 'react';
import {Button, TextField, Link, Grid, Box, Typography, Container} from '@material-ui/core/';
import Alert from '@material-ui/lab/Alert';

import {connect} from 'react-redux';
import {reduxForm, Field} from "redux-form"
import * as actions from './duck/AuthAction';
import * as validations from './duck/AuthValidation';

import CssBaseline from '@material-ui/core/CssBaseline';
import {withStyles} from '@material-ui/core/styles';

import Copyright from '../common/copyright/Copyright';
import signupBg from '../../assets/img/auth-bg.jpg';
import adwMainLogo from '../../assets/img/logo/adw-main-logo-70.svg';

const styles = theme => ({
    paper: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    form: {
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(2, 0, 1),
    },
    mainBg: {
        backgroundImage: `url(${signupBg})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        display: 'flex',
        justifyContent: 'center', 
        alignItems: 'center', 
        flexDirection: 'column',
        height: '100vh'
    }
});

const fields = {
    siret: 'siret',
    email: 'email',
    password: 'password',
    confirmPassword: 'confirmPassword'
}

class SignupPage extends Component {
    handleSubmit = formValues => {
        console.log(formValues);
        this.props.signupUser(formValues, this.props.history);
    }

    renderInputComponent = field => {
        return (
            <div>
                <TextField
                {...field.input}
                label={field.label}
                type={field.type}
                variant="outlined"
                margin="normal"
                fullWidth
                />
                {field.meta.touched && field.meta.error
                    && <Alert severity="error">{field.meta.error}</Alert>
                }
            </div>
        );
    }

    render() {
        const {classes, handleSubmit} = this.props;

        return (
            <React.Fragment>
                <div className={classes.mainBg}>
                    <Container component="main" maxWidth="xs">
                        <CssBaseline />
                        <div className={classes.paper}>
                            <Link to="/">
                                <Button href="/">
                                    <img src={adwMainLogo} width="230" height="130" />
                                </Button>
                            </Link>
                            <Typography component="h1" variant="h5">Inscription</Typography>
                            <form className={classes.form} onSubmit={handleSubmit(this.handleSubmit)}>
                                <Grid container spacing={2}>
                                    <Grid item xs={12}>
                                        <Field
                                            name={fields.siret}
                                            type="text"
                                            label="SIRET"
                                            id="siret"
                                            component={this.renderInputComponent}
                                        />
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Field
                                            name={fields.email}
                                            type="email"
                                            label="Adresse e-mail"
                                            component={this.renderInputComponent}
                                            
                                        />
                                    </Grid>
                                    <Grid item xs={12} xs={6}>
                                        <Field
                                            name={fields.password}
                                            type="password"
                                            label="Mot de passe"
                                            component={this.renderInputComponent}
                                        />
                                    </Grid>
                                    <Grid item xs={12} xs={6}>
                                        <Field
                                            name={fields.confirmPassword}
                                            type="password"
                                            label="Confirmation"
                                            component={this.renderInputComponent}
                                        />
                                    </Grid>
                                </Grid>
                                <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}
                                >
                                    Inscription
                                </Button>
                                <Grid container justify="flex-end">
                                    <Grid item>
                                        <Link href="/signin" variant="body2">
                                            Vous êtes déjà inscrit ? Connectez-vous !
                                        </Link>
                                    </Grid>
                                </Grid>
                            </form>
                        </div>
                        <Box mt={8}>
                            <Copyright />
                        </Box>
                    </Container>
                </div>
            </React.Fragment>
        );
    }
}

function validate(formValues) {
    const errors = {};
    // formValues.xxx = valeur dans le store
    errors.siret = validations.validateNotEmpty(formValues.siret);
    errors.email = validations.validateEmail(formValues.email);
    errors.password = validations.validateNotEmpty(formValues.password);
    errors.confirmPassword = validations.validateEqual(formValues.password, formValues.confirmPassword);

    return errors;
}

const signupForm = reduxForm({
    form: 'signup',
    fields: Object.keys(fields),
    validate
})(SignupPage);

export default withStyles(styles)(connect(null, actions)(signupForm));