import axios from 'axios';
import {BASE_URL} from '../../../Constants';
import {SET_AUTHENTIFICATION} from './AuthTypes';
import { parseError } from '../../common/error/duck/ErrorAction';

export function setAuthentification(isLoggedIn) {
    return {
        type: SET_AUTHENTIFICATION,
        payload: isLoggedIn
    };
}

/**
history dispo dans les props
*/
export function signinUser({username, password}, history) {
    return function(dispatch) {
        axios.post(`${BASE_URL}/login_check`, {
            username,
            password
        })
        .then((res) => {
            console.log(res);
            localStorage.setItem("token", res.data.token);
            dispatch(setAuthentification(true));
            history.push('/dashboard');
        }).catch((err) => {
            dispatch(parseError('identifiants invalides'));
        })
    };
}

export function signupUser({email, password, siret}, history) {
    return function(dispatch) {
        axios.post(`${BASE_URL}/users`, {
            email,
            password,
            siret
        })
        .then((res) => {
            localStorage.setItem("token", res.data.token);
            dispatch(setAuthentification(true));
            history.push("/dashboard");
        }).catch((err) => {
            console.log(err);
        })
    };
}

export function signoutUser() {
    return function(dispatch) {
        dispatch(setAuthentification(false))
        localStorage.removeItem("token");
    }
}