import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as actions from './duck/AuthAction';
import { Typography } from '@material-ui/core';
import Header from '../common/header/Header';

class Signout extends Component {
    componentWillMount() {
        this.props.signoutUser();
    }
    
    render() {
        return (
            <React.Fragment>
                <Header />
                <Typography variant="h2">
                    Vous avez été déconnecté.
                </Typography>
            </React.Fragment>
        )
    }
}

export default connect(null, actions)(Signout);