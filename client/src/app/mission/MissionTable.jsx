import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {Table, Button, TableBody, TableCell, TableContainer, TableHead, TableRow} from '@material-ui/core/';
import {Link} from 'react-router-dom';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function createData(id, title, description, nbValidatePoints, state, dateCreation, dateUpdate, actions) {
  return {id, title, description, nbValidatePoints, state, dateCreation, dateUpdate, actions};
}

const rows = [
    createData('10', 'Création site web pour Groupe 148', 'Consectetur adipiscing elit. Donec rhoncus mi sit amet massa auctor...', 4, 'En cours', '06/03/2020', '06/03/2020', ''),
    createData('11', 'Création site web pour Groupe 148', 'Consectetur adipiscing elit. Donec rhoncus mi sit amet massa auctor...', 4, 'En cours', '06/03/2020', '06/03/2020', ''),
];

export default function MissionTable() {
    const classes = useStyles();

    return (
        <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell>#</TableCell>
                        <TableCell>Titre</TableCell>
                        <TableCell>Description</TableCell>
                        <TableCell>Points de validation</TableCell>
                        <TableCell>Etat</TableCell>
                        <TableCell>Date création</TableCell>
                        <TableCell>Dernière mise à jour</TableCell>
                        <TableCell>Actions</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                {rows.map(row => (
                    <TableRow key={row.name}>
                        <TableCell>{row.id}</TableCell>
                        <TableCell>{row.title}</TableCell>
                        <TableCell>{row.description}</TableCell>
                        <TableCell>{row.nbValidatePoints}</TableCell>
                        <TableCell>{row.state}</TableCell>
                        <TableCell>{row.dateCreation}</TableCell>
                        <TableCell>{row.dateUpdate}</TableCell>
                        <TableCell>
                        {/* <Link to={`/missions/${row.id}`}> */}
                            <Button href="/missions/10" variant="contained" color="primary">
                                    Voir détails
                            </Button>
                        {/* </Link> */}
                        </TableCell>
                    </TableRow>
                ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}