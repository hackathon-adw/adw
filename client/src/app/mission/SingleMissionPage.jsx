import React, { Component } from 'react';
import Header from '../common/header/Header';
import Mission from './Mission';

class SingleMissionPage extends Component {
    render() {
        return (
            <React.Fragment>
                <Header />
                <Mission />
            </React.Fragment>
        );
    }
}

export default SingleMissionPage;