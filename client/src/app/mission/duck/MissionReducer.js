import {
    GET_MISSIONS,
    GET_MISSION,
    POST_MISSIONS,
    DELETE_MISSION
} from './MissionTypes';

const initialState = {
    missions: []
};

export default function(state = initialState, action) {
    switch(action.type) {
        case GET_MISSIONS:
            return action.payload;
        case DELETE_MISSION:
            return state.filter((mission) => {
                if(mission.id === action.payload) {
                    return false;
                } else {
                    return true;
                }
            })
        case POST_MISSIONS:
            return [...state, action.payload];
        default:
    }

    return state;
}