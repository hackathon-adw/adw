import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import sectionOne from '../../assets/img/sectionOne.png';

const useStyles = makeStyles(theme => ({
    cardHeader: {
        backgroundColor:
        theme.palette.type === 'dark' ? theme.palette.grey[700] : theme.palette.grey[200],
    },
    mainBg: {
        backgroundImage: `url(${sectionOne})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        display: 'flex',
        justifyContent: 'center', 
        alignItems: 'center', 
        flexDirection: 'column',
        height: '100vh'
    }
}));

const tiers = [
    {
        title: 'Les bonnes pratiques chez 148',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus gravida odio leo, id hendrerit ex ornare vitae. Nullam elit dolor, blandit quis urna id, tincidunt vehicula odio. Vestibulum at efficitur nunc. In fermentum purus eget felis viverra, eu ultricies tortor hendrerit. Proin tortor nibh, suscipit eget laoreet vitae, cursus quis arcu.',
        mdGrid: 6
    },
    {
        title: 'Les avantages à être Agent',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus gravida odio leo, id hendrerit ex ornare vitae. Nullam elit dolor, blandit quis urna id, tincidunt vehicula odio. Vestibulum at efficitur nunc. In fermentum purus eget felis viverra, eu ultricies tortor hendrerit. Proin tortor nibh, suscipit eget laoreet vitae, cursus quis arcu.',
        mdGrid: 6
    },
    {
        title: 'Auto entrepreneuriat',
        description: 'Nunc vitae dictum nisi. Aliquam blandit consectetur dictum. Sed porta finibus est. Cras accumsan orci sed lectus dignissim, non auctor ante sagittis. Nullam eu nibh vitae dolor feugiat faucibus nec sit amet odio. Proin hendrerit porta nisl non sodales. Ut sollicitudin at massa quis porta. Phasellus pretium diam et consectetur euismod. Aenean id vehicula tortor, non accumsan velit. Etiam at ipsum iaculis, suscipit lectus sit amet, consectetur est. Pellentesque pretium magna vel justo luctus mollis. Integer malesuada iaculis odio, hendrerit tincidunt metus bibendum vitae. Nunc vel aliquam turpis. Proin vehicula nibh quis sapien pretium euismod. Proin arcu nibh, ornare ac semper ac, vestibulum vel enim.',  
        mdGrid: 12
    },
];

export default function Pricing() {
    const classes = useStyles();

    return (
        <React.Fragment>
            <div className={classes.mainBg}>
                <Container maxWidth="md" component="main">
                    <Grid container spacing={5} alignItems="flex-end">
                    {tiers.map(tier => (
                        <Grid item key={tier.title} xs={12} md={tier.mdGrid}>
                        <Card>
                            <CardHeader
                            title={tier.title}
                            titleTypographyProps={{ align: 'center' }}
                            subheaderTypographyProps={{ align: 'center' }}
                            className={classes.cardHeader}
                            />
                            <CardContent>
                                <Typography variant="subtitle1" align="center">
                                    {tier.description}
                                </Typography>
                            </CardContent>
                        </Card>
                        </Grid>
                    ))}
                    </Grid>
                </Container>
            </div>
        </React.Fragment>
    );
}